#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <numeric>

class Trip
{
    std::vector<int> Expense;
    int N;

public:
    Trip(int _N) : N{_N} { }

    void GetExpense()
    {
        Expense = std::vector<int>(N);
        for (int i = 0; i < N; ++i)
        {
            int Dollars, Cents;
            char Dot;
            std::cin >> Dollars >> Dot >> Cents;

            Expense[i] = Dollars * 100 + Cents;
        }
    }

    double ComputeMoney()
    {
        int Cost = std::accumulate(Expense.begin(), Expense.end(), 0);
        double CPP = static_cast<double>(Cost) / N;

        double MoneyIn = 0.0;
        double MoneyOut = 0.0;

        for (int i = 0; i < N; ++i)
        {
            double Difference = Expense[i] - CPP;
            if (Difference < 0.0)
            {
                MoneyIn -= static_cast<int>(Difference) / 100.0;
            }
            else
            {
                MoneyOut += static_cast<int>(Difference) / 100.0;
            }
        }

        return std::max(MoneyOut, MoneyIn);
    }
};

int main()
{
    std::vector<double> Cost;

    while (true)
    {
        int N;
        std::cin >> N;
        if (N == 0) break;

        Trip T(N);

        T.GetExpense();
        double Money = T.ComputeMoney();
        Cost.push_back(Money);
    }

    for (int i = 0; i < Cost.size(); ++i)
    {
        std::cout << "$" << std::fixed << std::setprecision(2)
                  << Cost[i] << std::endl;
    }

    return 0;
}
