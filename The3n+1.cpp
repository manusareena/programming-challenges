#include <iostream>

int ComputeCycleLength(int N)
{
    int Count = 1;

    while (N != 1)
    {
        ++Count;
        if ((N & 1) == 0)
        {
            N >>= 1;
        }
        else
        {
            N = 3 * N + 1;
        }
    }

    return Count;
}

int main()
{
    int M, N;
    while (std::cin >> M >> N)
    {
        int P = M;
        int Q = N;
        if (M > N)
        {
            int Temp = M;
            M = N;
            N = Temp;
        }

        int MaxCycleLength = 0;
        for (int i = M; i <= N; ++i)
        {
            MaxCycleLength = std::max(MaxCycleLength, ComputeCycleLength(i));
        }
        std::cout << P << " " << Q << " " << MaxCycleLength << std::endl;
    }

    return 0;
}
