#include <iostream>
#include <vector>

class MineField
{
    static int FieldMapNum;
    int MapNum;
    int N, M;
    std::vector<std::vector<int> > MineMap;
    std::vector<std::vector<char> > FieldMap;

public:
    MineField(int _N, int _M) : N{_N}, M{_M}, MapNum{++FieldMapNum} { }

    void GetFieldMap()
    {
        FieldMap = std::vector<std::vector<char> >(N, std::vector<char>(M));

        for (int i = 0; i < N; ++i)
        {
            for (int j = 0; j < M; ++j)
            {
                std::cin >> FieldMap[i][j];
            }
        }
    }

    void ComputeMineMap()
    {
        MineMap = std::vector<std::vector<int> >(N, std::vector<int>(M));

        for (int i = 0; i < N; ++i)
        {
            for (int j = 0; j < M; ++j)
            {
                if (FieldMap[i][j] == '*')
                {
                    MineMap[i][j] = -1;

                    for (int p = -1; p <= 1; ++p)
                    {
                        int r = i + p;
                        if (r < 0 || r > N - 1) continue;

                        for (int q = -1; q <= 1; ++q)
                        {
                            int c = j + q;
                            if (c < 0 || c > M - 1) continue;
                            if (FieldMap[r][c] != '*')
                            {
                                ++MineMap[r][c];
                            }
                        }
                    }
                }
            }
        }
    }

    void PrintMineMap()
    {
        std::cout << "Field #" << FieldMapNum << ":\n";
        for (int i = 0; i < N; ++i)
        {
            for (int j = 0; j < M; ++j)
            {
                if (MineMap[i][j] == -1)
                {
                    std::cout << '*';
                }
                else
                {
                    std::cout << MineMap[i][j];
                }
            }
            std::cout << "\n";
        }
    }
};

int MineField::FieldMapNum = 0;

int main()
{
    int N, M;
    std::cin >> N >> M;
    while (N != 0)
    {
        MineField Field(N, M);

        Field.GetFieldMap();
        Field.ComputeMineMap();
        Field.PrintMineMap();
        std::cin >> N >> M;
        if (N == 0) break;
        std::cout << std::endl;
    }

    return 0;
}
